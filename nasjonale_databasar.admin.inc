<?php
// $Id$


header('Content-Type: text/html; charset=utf-8');

function nasjonale_databasar_settings($form, &$form_state)
{
  $form['nasjonale_databasar_title'] = array(
    '#type' => 'textfield',
    '#title' => 'Page title',
    '#description' => t('Title of the search page'),
    '#default_value' => variable_get('nasjonale_databasar_title', t('Search page')),
   );
  $form['nasjonale_databasar_submit'] = array(
    '#type' => 'textfield',
    '#title' => 'Value for "submit" button',
    '#description' => t('The value displayed on the search button'),
    '#default_value' => variable_get('nasjonale_databasar_submit', t('Search')),
   );
  $form['nasjonale_databasar_toptext'] = array(
    '#type' => 'textarea',
    '#title' => 'Intro text',
    '#description' => t('If you want an intro text above the search forms, enter it here'),
    '#default_value' => variable_get('nasjonale_databasar_toptext', ''),
   );
  $form['nasjonale_databasar_boxes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('What search forms should be available'),
    '#default_value' => variable_get('nasjonale_databasar_boxes', array('kms' => 'kms', 'digmus' => 'digmus', 'nasjbib' => 'nasjbib', 'digark' => 'digark', 'musit' => 'musit', 'euro' => 'euro', 'arkport' => 'arkport')),
    '#required' => TRUE,
    '#options' => array('digmus' => 'Digitalt museum', 'nasjbib' => 'Nasjonalbiblioteket', 'digark' => 'Digitalarkivet', 'musit' => 'Musit', 'euro' => 'Europeana', 'arkport' => 'Arkivportalen', 'kms' => 'Kulturminnes&oslash;k'),
    '#description' => t('All checked services will be searchable through !here', array('!here' => '<a href="/nasjonale_databasar">here</a>'))
  );

  return system_settings_form($form);
}
